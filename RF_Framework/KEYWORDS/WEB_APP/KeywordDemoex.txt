*** Settings ***
Resource          ../Keywords.txt

*** Keywords ***
Login to App Demoex
    [Arguments]    ${USER}    ${Password}
    Set Library Search Order    SeleniumLibrary
    KEYWORD STATUS PENDING    Login to App Demoex
    [W] Open Browser    http://demoexex.esy.es/index.php
    [W] Click Element    //*[@title="Log in to your customer account"]
    [W] Input Text    //*[@class="form-control" and @name="email"]    ${USER}
    [W] Input Text    //*[@name="password"]    ${Password}
    [W] Click Element    //*[@id="submit-login"]
    KEYWORD STATUS END
    [Teardown]    KEYWORD STATUS TEARDOWN
